var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var kanaliZGeslom=[];
var gesla=[];

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZasebnoSporocilo(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
      izpisiUporabnike(socket);
    });
    
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function izpisiUporabnike(socket){
  var kanal=trenutniKanal[socket.id];
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  var besedilo='';
  for (var i in uporabnikiNaKanalu) {
    var uporabnikId=uporabnikiNaKanalu[i].id;
    besedilo=besedilo+vzdevkiGledeNaSocket[uporabnikId]+' ';
  }
  socket.broadcast.to(kanal).emit('uporabnik', {
    besedilo: besedilo
  });
  socket.emit('uporabnik', {
    besedilo: besedilo
  });
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  var imeUporabnika=vzdevkiGledeNaSocket[socket.id];
  socket.emit('pridruzitevOdgovor', {kanal: kanal, vzdevek: imeUporabnika});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });
  
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        var imeKanala=trenutniKanal[socket.id];
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal: imeKanala
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    if(kanal.geslo!=null){
      var obstaja=false;
      for(var i=0;i<kanaliZGeslom.length;i++){
        if(kanal.novKanal==kanaliZGeslom[i]){
          if(kanal.geslo==gesla[i]){
            socket.leave(trenutniKanal[socket.id]);
            pridruzitevKanalu(socket, kanal.novKanal);
            obstaja=true;
          }else{
            obstaja=true;
            socket.emit('sporocilo', {besedilo: 'Pridružitev v kanal ' + kanal.novKanal + ' ni bilo uspešno, ker je geslo napačno!'});
            break;
          }
        }
      }
    
      if(!obstaja){
        var kanalBrezgesla=false;
        for(var j in io.sockets.manager.rooms) {
          j = j.substring(1, j.length);
          if(j==kanal.novKanal){
            socket.emit('sporocilo', {besedilo: 'Izbrani kanal '+kanal.novKanal+' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <kanal> ali zahtevajte kreiranje kanala z drugim imenom.'});
            kanalBrezgesla=true;
          }
        }
        if(!kanalBrezgesla){
          socket.leave(trenutniKanal[socket.id]);
          pridruzitevKanalu(socket, kanal.novKanal);
          gesla.push(kanal.geslo);
          kanaliZGeslom.push(kanal.novKanal);
        }
      } 
    }else{
      var potrebujeGeslo=false;
      for(var i=0;i<kanaliZGeslom.length;i++){
        if(kanal.novKanal==kanaliZGeslom[i]){
          potrebujeGeslo=true;
        }
      }
      if(potrebujeGeslo){
        socket.emit('sporocilo', {besedilo: 'Pridružitev v kanal ' + kanal.novKanal + ' ni bilo uspešno, ker je geslo napačno!'});
      }else{
        socket.leave(trenutniKanal[socket.id]);
        pridruzitevKanalu(socket, kanal.novKanal);
      }
    }
  });
}


function obdelajZasebnoSporocilo(socket) {
  socket.on('zasebnoSporocilo', function(rezultat) {
    var uporabniki = io.sockets.clients();
    var obstaja = false;
    var tekst='';
  
    for(var i in uporabniki){
      var uporabnikSocketId = uporabniki[i].id;
      if(rezultat.uporabnik == vzdevkiGledeNaSocket[uporabnikSocketId]){
        if(rezultat.uporabnik != vzdevkiGledeNaSocket[socket.id]){
        tekst=vzdevkiGledeNaSocket[socket.id]+' (zasebno): '+rezultat.sporocilo;
        uporabniki[i].emit('sporocilo', {besedilo: tekst});
        tekst='(zasebno za ' + rezultat.uporabnik + '): ' + rezultat.sporocilo;
        socket.emit('sporocilo', {besedilo: tekst});
        obstaja = true;
        }
      }
    }

    if(!obstaja){
      tekst='Sporočilo "' + rezultat.sporocilo + '" uporabniku z vzdevkom ' + rezultat.uporabnik + ' ni bilo mogoče posredovati.';
      socket.emit('sporocilo', {besedilo: tekst});
    }
  });
}


setInterval(function() {
    var kanali = io.sockets.manager.rooms;
    var izbrisan=true;
    for(var i=0;i<kanaliZGeslom.length;i++){
      for(var j in kanali){
        j = j.substring(1, j.length);
        if(kanaliZGeslom[i]==j){
          izbrisan=false;
        }
      }
      if(izbrisan){
        delete kanaliZGeslom[i];
        delete gesla[i];
      }
    }
}, 1000);


function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}