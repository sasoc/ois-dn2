var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal,geslo) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: geslo
  });
};

Klepet.prototype.posljiZasebnosporocilo = function(sporocilo, uporabnik) {
  this.socket.emit('zasebnoSporocilo', {
    sporocilo: sporocilo,
    uporabnik: uporabnik
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      var geslo='';
      var kanal='';
      besede.shift();
      if(besede[0].charAt(0)=='"'){
        kanal=besede[0].substring(1, besede[0].length-1);
        besede.shift();
        geslo = besede.join(' ');
        geslo = geslo.substring(1, geslo.length-1);
        this.spremeniKanal(kanal,geslo);
      }else{
        kanal = besede.join(' ');
        this.spremeniKanal(kanal,null);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      if(besede[0].charAt(0)=='"' && besede[0].charAt(besede[0].length-1)=='"'){
        var uporabnik = besede[0].substring(1, besede[0].length-1);
        besede.shift();
        var privat =besede.join(' ');
        if(privat.charAt(0)=='"' && privat.charAt(privat.length-1)=='"'){
          privat=privat.substring(1, privat.length-1);
          this.posljiZasebnosporocilo(privat, uporabnik);
        }else{
          sporocilo = 'Neznan ukaz.';
        }
      }else{
        sporocilo = 'Neznan ukaz.';
      }
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};