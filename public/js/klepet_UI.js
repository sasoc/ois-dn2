function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function spremeniEmoje(sporocilo){
  for(var j=0;j<sporocilo.length;j++){
     sporocilo=sporocilo.replace('<','&lt');
     sporocilo=sporocilo.replace('>','&gt');
  }
  for(var i=0;i<sporocilo.length;i++){
      sporocilo=sporocilo.replace(';)','<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/> ');
      sporocilo=sporocilo.replace(':)','<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/> ');
      sporocilo=sporocilo.replace('(y)','<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/> ');
      sporocilo=sporocilo.replace(':*','<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"/> ');
      sporocilo=sporocilo.replace(':(','<img src = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/> ');
  }
      
      return sporocilo;
}

function swearWords(sporocilo){
  var kletvice = $('#kletvice').text();
  var tabela = kletvice.split('\n');
  
  var tabela1=sporocilo.split(' ');
  for(var i=0;i<tabela.length;i++){
    var zvezdice1='';
    for(var k=0;k<tabela[i].length;k++){
      zvezdice1 = zvezdice1 + '*';
    }
    for(var j=0;j<tabela1.length;j++){
      if(tabela[i].toLowerCase()==tabela1[j].toLowerCase()){
         tabela1[j]=tabela1[j].replace(tabela1[j],zvezdice1);
      }
    }
  }
  sporocilo=tabela1.join(' ');
  return sporocilo;
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {

    sporocilo=swearWords(sporocilo);
    var novoBesedilo=spremeniEmoje(sporocilo);
    
    if(sporocilo == novoBesedilo){
      var index =$('#kanal').text().indexOf('@');
      var kanalUporabnika=$('#kanal').text();
      kanalUporabnika=kanalUporabnika.substring(index+2, kanalUporabnika.length);
      klepetApp.posljiSporocilo(kanalUporabnika, sporocilo);
      $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    }else{
      var index1 =$('#kanal').text().indexOf('@');
      var kanalUporabnika1=$('#kanal').text();
      kanalUporabnika1=kanalUporabnika1.substring(index1+2, kanalUporabnika1.length);
      klepetApp.posljiSporocilo(kanalUporabnika1, sporocilo);
      $('#sporocila').append($('<div style="font-weight: bold"></div>').html(novoBesedilo));
    }

     $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  $('#kletvice').load('swearWords.txt');

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#kanal').text(rezultat.vzdevek + ' @ ' + rezultat.kanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.vzdevek + ' @ ' + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    
    sporocilo.besedilo=swearWords(sporocilo.besedilo);
    var novoBesedilo=spremeniEmoje(sporocilo.besedilo);
    
    var novElement;
    if(sporocilo.besedilo == novoBesedilo){
      novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    }else{
      novElement = $('<div style="font-weight: bold"></div>').html(novoBesedilo);
    }
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabnik', function(uporabniki) {
     $('#seznam-uporabnikov').empty();
    var tabela=uporabniki.besedilo.split(' ');
    for(var i=0;i<tabela.length;i++){
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(tabela[i]));
    }
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  
  $('kanal').text(kletvice);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});